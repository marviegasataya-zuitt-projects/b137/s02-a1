package b137.gasataya.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args) {
        System.out.println("Leap year Calculator\n");

        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is your firstname?\n");
        String firstName = appScanner.nextLine();
        System.out.println("Hello, " + firstName + "\n");

        System.out.println("Enter a year:\n");
        int year = appScanner.nextInt();

        boolean isDivisibleBy4 = false;

        if (year % 4 != 0) {
            System.out.println("The year " + year + " is a NOT leap year. \n");
        } else {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    System.out.println("The year " + year + " is a leap year. \n");
                } else {
                    System.out.println("The year " + year + " is a NOT a leap year. \n");
                }
            } else {
                System.out.println("The year " + year + " is a leap year. \n");
            }
        }

    }


}
